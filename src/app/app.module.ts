import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './crud/user-list/user-list.component';
import { EditUserComponent } from './crud/edit-user/edit-user.component';
import { DeleteUserComponent } from './crud/delete-user/delete-user.component';
import { CreateUserComponent } from './crud/create-user/create-user.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AddUserComponent } from './curd/add-user/add-user.component';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    EditUserComponent,
    DeleteUserComponent,
    CreateUserComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AddUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
