import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserCurdService {

  constructor(public http: HttpClient) { }
   // larvelApi = 'http://localhost:8000/api/v1/';
   larvelApi = 'http://localhost:4300/api/';

  addUser( obj ) {
    return this.http.post(this.larvelApi + 'create-user', obj);
  }

  userlist() {
    return this.http.get(this.larvelApi + 'user-list');
  }

  edituser(id){
    return this.http.get(this.larvelApi + 'get-user/?uid=' + id);
  }
  updateuser( id , obj) {
    return this.http.put(this.larvelApi + 'update-user/?uid=' + id, obj);
  }
  deleteuser( id ) {
    return this.http.delete(this.larvelApi + 'delete-user/?uid=' + id);
  }
}
