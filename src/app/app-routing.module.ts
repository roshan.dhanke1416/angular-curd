import { AddUserComponent } from './curd/add-user/add-user.component';
import { DeleteUserComponent } from './crud/delete-user/delete-user.component';
import { EditUserComponent } from './crud/edit-user/edit-user.component';
import { UserListComponent } from './crud/user-list/user-list.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'',component : UserListComponent},
  {path:'edit-user',component : EditUserComponent},
  {path:'delete-user',component : DeleteUserComponent},
  {path:'add-user',component : AddUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
