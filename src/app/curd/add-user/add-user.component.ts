import { element } from 'protractor';
import { UserCurdService } from './../../services/user-curd.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RouterLink, Router } from '@angular/router';
import { exit } from 'process';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private fb: FormBuilder, private user: UserCurdService, private router: Router) { }
  addUser = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
    age: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
  });

  showError = {
    show: false,
    message: ''
  };
  showSuc = {
    show: false,
    message: ''
  };
  uploadedFiles: File;

  ngOnInit() {
  }

  crateUser() {

    // this.addUser.value.file = this.uploadedFiles;
    // console.log(this.addUser.value);
    // const object: any = this.addUser.value;

    // const formData: FormData = new FormData();
    // formData.append('file', this.uploadedFiles, this.uploadedFiles.name);
    // console.log(formData);
    const object: any = this.addUser.value;
    console.log(object);
    this.user.addUser(object).subscribe(
      (res: any) => {

        if (res.status) {
          this.showSuc.show = true;
          this.showSuc.message = 'User Created Successfuly';
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 2000);
        } else {
          this.showError.show = true;
          this.showError.message = 'Some thing went wrong please try again';
          setTimeout(() => {
            this.restError();
          }, 2000);
        }
      },
    );
  }

  restError() {
    this.showError = {
      show: false,
      message: ''
    };
  }

  fileChange(file: FileList ) {
    console.log(this.uploadedFiles = file.item(0));
  }


}
