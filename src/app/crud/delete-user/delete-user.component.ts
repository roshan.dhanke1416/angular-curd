import { UserCurdService } from './../../services/user-curd.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  constructor(public user: UserCurdService, private route: ActivatedRoute, private router: Router) { }
  params: any;
  ngOnInit() {
    this.route.queryParams.subscribe(
      (res) => {
        this.params = res;
      }
    );
    this.user.deleteuser(this.params.id).subscribe( (res: any ) => {
      if (res.status === 1) {
        this.router.navigate(['/']);
      } else {
        // alert('Record not found');
        this.router.navigate(['/']);
      }
    });
  }

}
