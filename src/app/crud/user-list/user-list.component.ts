import { UserCurdService } from './../../services/user-curd.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(public  user: UserCurdService) { }
  userlist: any;
  ngOnInit() {
    this.user.userlist().subscribe(
      (res: any) => {
        this.userlist = res.result;
        console.log( this.userlist );
      }
    );
  }
}
