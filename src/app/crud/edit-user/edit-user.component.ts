import { UserCurdService } from './../../services/user-curd.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { RouterLink, Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  constructor(private fb: FormBuilder, private user: UserCurdService, private router: Router, private route: ActivatedRoute) { }

  addUser = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
    age: ['', [Validators.required, Validators.min(1), Validators.max(100)]],
  });

  params: any;

  showError = {
    show: false,
    message: ''
  };
  showSuc = {
    show: false,
    message: ''
  };
  userdata: any;

  ngOnInit() {
   this.route.queryParams.subscribe(
      (res) => {
        this.params = res;//?id = 1
        this.getUserDetails(res.id);
      }
    );
  }

  getUserDetails( id ) {
    this.user.edituser(id).subscribe((res: any) => {
      this.userdata = res.result;
    });
  }
  updateuser() {
    const object: any = this.addUser.value;
    this.user.updateuser(this.params.id, object).subscribe(
      (res: any) => {
        this.showSuc.show = true;
        this.showSuc.message = 'User update Successfuly';
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 2000);
      },
    );
  }

  restError() {
    this.showError = {
      show: false,
      message: ''
    };
  }

}
